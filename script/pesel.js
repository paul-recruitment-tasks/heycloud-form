/**
 * Class Pesel contains three properties:
 * 
 * pesel        - pesel provided in constructor
 * dateOfBirth  - date of birth calculated during construction
 * isValid      - true if pesel is valid, false otherwise
 */
export default class Pesel {
    /**
     * Use parametrized constructor if you want to obtain full fledged Pesel objects to constatly have access to data extracted from pesel
     * @param {*} pesel input pesel to be decoded and verified
     */
    constructor(pesel) {
        let isValidLengthAndDigits = false
        this.pesel = pesel.toString()
        if (this.validateLengthAndDigits()) {
            this.dateOfBirth = this.calculateDateOfBirth()
            isValidLengthAndDigits = true
        }
        this.isValid = isValidLengthAndDigits
            && this.validateControlSum()
            && !!this.dateOfBirth
    }

    // Methods
    validateLengthAndDigits() {
        const elevenDigits = /^[0-9]{11}$/
        if (!elevenDigits.test(this.pesel)) {
            return false
        }
        return true
    }

    validateControlSum() {
        const weight = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3]
        const controlNumber = parseInt(this.pesel.substring(10, 11))
        let sum = 0


        for (let i = 0; i < weight.length; i++) {
            sum += (parseInt(this.pesel.substring(i, i + 1)) * weight[i])
        }
        sum = sum % 10
        return (10 - sum) % 10 === controlNumber
    }

    calculateDateOfBirth() {
        // extract year, month, day values to process
        let year = parseInt(this.pesel.substring(0, 2))
        let month = parseInt(this.pesel.substring(2, 4))
        let day = parseInt(this.pesel.substring(4, 6))

        // calculate year and month
        if (month < 20) {
            year += 1900
        } else if (month < 40) {
            year += 2000
            month -= 20
        } else if (month < 60) {
            year += 2100
            month -= 40
        } else if (month < 80) {
            year += 2200
            month -= 60
        } else {
            year += 1800
            month -= 80
        }

        // check if calculated month is valid, if not return false
        if (!this.isValidMonth(month)) {
            return false
        }

        // check if extracted day is valid for calculated month, if not return false
        if (!this.isValidDayOfMonth(year, month, day)) {
            return false
        }

        // return valid date of birth extracted form pesel
        return new Date().setFullYear(year, month - 1, day)
    }

    isValidMonth(month) {
        return month > 0 && month <= 12
    }

    isValidDayOfMonth(year, month, day) {
        let monthsLengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

        // Adjust monthLengths array for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthsLengths[1] = 29
        }

        // Check the range of the day
        return day > 0 && day <= monthsLengths[month - 1]
    }
}