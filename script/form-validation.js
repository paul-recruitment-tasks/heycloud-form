import Pesel from './pesel.js'

// Get form fields
const form = document.getElementById('form')
const formMessage = document.getElementById('form-message')

const PeselInput = document.getElementById('pesel')
const dateOfBirth = document.getElementById('date-of-birth')

// Listner that tries to fill in date when 11 pesel chars provided
PeselInput.addEventListener('input', (event) => {
    let inputLength = event.target.value.length
    if (inputLength === 11) {
        const pesel = new Pesel(event.target.value)
        if (pesel.isValid) {
            dateOfBirth.value = new Date(pesel.dateOfBirth).toISOString().substring(0, 10)
        } else {
            dateOfBirth.value = null
        }
    }
})

// Submit form listener
form.addEventListener('submit', (event) => {
    event.preventDefault()
    const pesel = new Pesel(PeselInput.value)
    if (!pesel.isValid) {
        openModal("błąd")
    } else {
        openModal("sukces")
    }
})

function openModal(newModalContent) {
    const modal = document.getElementById("modal")
    const modalContent = document.getElementById("modal-content")
    const closeButton = document.getElementById("close-modal")
    // Open modal
    modalContent.innerHTML = newModalContent
    modal.style.display = "block"
    // When the user clicks on <span> (x), close the modal
    closeButton.onclick = function () {
        modal.style.display = "none"
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none"
        }
    }
}